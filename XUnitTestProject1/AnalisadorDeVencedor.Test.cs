using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTestProject1
{

    // No jogo de Poker, uma m�o consiste em cinco cartas que podem ser comparadas, da mais baixa para a mais alta, da seguinte maneira:
    // Carta Alta: A carta de maior valor.
    // Um Par: Duas cartas do mesmo valor.
    // Flush: Todas as cartas do mesmo naipe

    // As cartas devem ser validas
    // As cartas sao, em ordem crescente de valor: 2, 3, 4, 5, 6, 7, 8, 9, 10, Valete, Damas, Reis, �s
    // Os naipes sao: Ouro (O), Copa (C), Espadas (E), Paus (C)

    public class AnalisadorDeVencedorTeste
    {
        [Theory]
        [InlineData("3O,5C,2E,8C,7P", "2O,4C,3P,6C,9C", "Segundo Jogador")]
        [InlineData("3O,5C,2E,9C,7P", "2O,4C,3P,6C,8C", "Primeiro Jogador")]
        [InlineData("3O,5C,2E,8C,7P", "2O,4C,3P,6C,10C", "Segundo Jogador")]
        [InlineData("3O,5C,2E,8C,VP", "2O,4C,3P,6C,AC", "Segundo Jogador")]
        public void Deve_analisar_vencedor_quando_tiver_maior_carta(string cartasPrimeiroJogador, string cartasSegundoJogador, string vencedorEsperado)
        {
            var cartasDoPrimeiroJogador = cartasPrimeiroJogador.Split(",").ToList();
            var cartasDoSegundoJogador = cartasSegundoJogador.Split(",").ToList();
            var analisador = new AnalisadorDeVencedor();

            var vencedor = analisador.Analisar(cartasDoPrimeiroJogador, cartasDoSegundoJogador);

            Assert.Equal(vencedorEsperado, vencedor);
        }
    }

    public class AnalisadorDeVencedor
    {
        public string Analisar(List<string> cartasDoPrimeiroJogador, List<string> cartasDoSegundoJogador)
        {
            var maiorCartaDoPrimeiroJogador = cartasDoPrimeiroJogador.Select(carta => ConverterParaValorDaCarta(carta))
                .OrderBy(valorDaCarta => valorDaCarta)
                .Max();

            var maiorCartaDoSegundoJogador = cartasDoSegundoJogador.Select(carta => ConverterParaValorDaCarta(carta))
                .OrderBy(valorDaCarta => valorDaCarta)
                .Max();

            return maiorCartaDoPrimeiroJogador > maiorCartaDoSegundoJogador ? "Primeiro Jogador" : "Segundo Jogador";
        }

        private int ConverterParaValorDaCarta(string carta)
        {
            var valorDaCarta = carta.Substring(0, carta.Length - 1);

            if (!int.TryParse(valorDaCarta, out var valor))
            {
                switch (valorDaCarta)
                {
                    case "V":
                        valor = 11;
                        break;
                    case "D":
                        valor = 12;
                        break;
                    case "R":
                        valor = 13;
                        break;
                    case "A":
                        valor = 14;
                        break;
                }
            }
            return valor;
        }
    }
}
